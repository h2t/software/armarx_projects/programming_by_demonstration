/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    programming_by_demonstration::ArmarXObjects::rule_based_biar
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Component.h"

#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/aron/core/data/variant/All.h>
#include <RobotAPI/libraries/armem/client/query/Builder.h>
#include <RobotAPI/libraries/armem_objects/aron/ObjectInstance.aron.generated.h>
#include <VisionX/libraries/armem_human/aron/BODY_25Pose.aron.generated.h>

#include <SimoxUtility/math/convert.h>
#include <deque>
#include <programming_by_demonstration/core/types.h>
#include <ArmarXCore/core/time.h>



namespace armarx::programming_by_demonstration::components::rule_based_biar
{

    const std::string
    Component::defaultName = "ttm_demo.rule_based_ar";


    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def = new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        def->optional(properties.referenceTimeEnabled, "ref_time.enabled", "Specifies whether to use the time offset or the actual current time.");
        def->optional(properties.referenceTime, "ref_time.value", "Time offset to use. Only considered if time_offset.enabled is true.");
        def->optional(properties.objectInstances.memoryName, "object_instances.memory_name", "Name of the memory to read object instances from.");
        def->optional(properties.objectInstances.coreSegmentName, "object_instances.core_segment_name", "Name of the core segment to read object instances from.");
        def->optional(properties.humanPoses.memoryName, "human_poses.memory_name", "Name of the memory to read human poses from.");
        def->optional(properties.humanPoses.coreSegmentName, "human_poses.core_segment_name", "Name of the core segment to read human poses from.");
        def->optional(properties.recognizedActions.memoryName, "recognized_actions.memory_name", "Name of the memory to write recognized actions to.");
        def->optional(properties.recognizedActions.coreSegmentName, "recognized_actions.core_segment_name", "Name of the pose core segment to write recognized actions to.");
        def->optional(properties.contactBelow, "contactBelow", "Threshold in [mm] below which to consider contact.");
        def->optional(properties.releasedAbove, "releasedAbove", "Threshold in [mm] above which to consider release.");

        return def;
    }


    void
    Component::onInitComponent()
    {
        ;
    }


    void
    Component::onConnectComponent()
    {
        ARMARX_IMPORTANT << "Waiting for memories ...";
        try
        {
            objectInstanceMemory = memoryNameSystem().useReader(properties.objectInstances.memoryName);
            humanPoseMemory = memoryNameSystem().useReader(properties.humanPoses.memoryName);
            recognizedActionsMemory = memoryNameSystem().useWriter(properties.recognizedActions);
        }
        catch (const armem::error::CouldNotResolveMemoryServer& e)
        {
            ARMARX_ERROR << e.what();
            return;
        }

        /* (Requies the armarx::DebugObserverComponentPluginUser.)
        // Use the debug observer to log data over time.
        // The data can be viewed in the ObserverView and the LivePlotter.
        // (Before starting any threads, we don't need to lock mutexes.)
        {
            setDebugObserverDatafield("numBoxes", properties.numBoxes);
            setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
            sendDebugObserverBatch();
        }
        */

        /* (Requires the armarx::ArVizComponentPluginUser.)
        // Draw boxes in ArViz.
        // (Before starting any threads, we don't need to lock mutexes.)
        drawBoxes(properties, arviz);
        */

        /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
        // Setup the remote GUI.
        {
            createRemoteGuiTab();
            RemoteGui_startRunningTask();
        }
        */
    }


    void
    Component::onDisconnectComponent()
    {
        ARMARX_DEBUG << "Disconnecting component.";

        ARMARX_DEBUG << "Done disconnecting component.";
    }


    void
    Component::onExitComponent()
    {
        ;
    }


    std::string
    Component::getDefaultName() const
    {
        return Component::defaultName;
    }


    std::string
    Component::GetDefaultName()
    {
        return Component::defaultName;
    }


    void
    Component::start(const Ice::Current&)
    {
        if (not task or not task->isRunning())
        {
            if (properties.referenceTimeEnabled)
            {
                timeOffset = armarx::Clock::Now() - armem::Duration::MicroSeconds(properties.referenceTime) + armem::Duration::Seconds(1);
            }

            currentSegment = ttm_demo::rule_based_ar::Segment();
            currentSegment.beginTime = 0;
            currentSegment.label = "none";
            segmentation = ttm_demo::rule_based_ar::Segmentation();
            segmentation.referenceTime = getCurrentTime().toMicroSeconds();

            task = new RunningTask<Component>(this, &Component::run);
            ARMARX_INFO << "Starting action recognition task.";
            task->start();
            ARMARX_INFO << "Started action recognition task.";
        }
        else
        {
            ARMARX_ERROR << "Action recognition task cannot be started as it is already running.";
        }
    }


    ttm_demo::rule_based_ar::Segmentation
    Component::stop(const Ice::Current&)
    {
        if (task and task->isRunning())
        {
            const bool join = true;
            ARMARX_INFO << "Stopping action recognition task.";
            task->stop(join);
            ARMARX_INFO << "Stopped action recognition task.";

            return segmentation;
        }
        else
        {
            ARMARX_ERROR << "Action recognition task cannot be stopped as it is not running.";
            return {};
        }
    }


    void
    Component::processObjectUpdates(const armem::MemoryID& subscriptionID, const std::vector<armem::MemoryID>& snapshotIDs)
    {
        ARMARX_DEBUG << "Got " << snapshotIDs.size() << " object pose data updates.";
    }


    void
    Component::run()
    {
        ARMARX_TRACE;

        ARMARX_DEBUG << "Starting running task.";

        armarx::CycleUtil cu{IceUtil::Time::milliSeconds(33)};

        armem::Time last_processed_human_pose;
        std::map<std::string, armem::Time> last_processed_objects;

        struct ObjectParams
        {
            float distance;
            float height;
            float width;
            float pitch;
        };

        std::map<std::string, ObjectParams> object_params_hist;
        auto add_object_params = [&object_params_hist](const std::string& name, ObjectParams object_params)
        {
            object_params_hist[name] = object_params;
        };
        auto get_filtered_object_params = [&object_params_hist](const std::string& name)
        {
            return object_params_hist[name];
        };
        auto add_and_filter_object_params =
            [&get_filtered_object_params, &add_object_params]
            (const std::string& name, ObjectParams new_object_params)
            -> std::tuple<ObjectParams, ObjectParams>
        {
            ObjectParams old_filtered_object_params = get_filtered_object_params(name);
            add_object_params(name, new_object_params);
            ObjectParams new_filtered_object_params = get_filtered_object_params(name);
            return {old_filtered_object_params, new_filtered_object_params};
        };
        auto action_to_string = [](const std::optional<core::ManipulationAction>& action) -> std::string {
            if (action.has_value() and action->action.name != "none")
            {
                return action->action.name + " "
                    + action->manipulatedObject.dataset() + "/"
                    + action->manipulatedObject.className();
            }
            return "none";
        };

        armarx::ObjectID manipulated_object;
        std::optional<core::ManipulationAction> current_action;

        while (not task->isStopped())
        {
            cu.waitForCycleDuration();

            ARMARX_TRACE;

            const armem::Time now = getCurrentTime();
            ReadMemoriesResult data = readMemories(now);

            ARMARX_TRACE;

            std::optional<human::arondto::Body25Pose3D> humanPose;
            {
                data.humanPoses.forEachInstance(
                    [&humanPose, &last_processed_human_pose](const armem::wm::EntityInstance& i)
                    {
                        if (i.metadata().timeCreated > last_processed_human_pose)
                        {
                            last_processed_human_pose = i.metadata().timeCreated;
                            humanPose = human::arondto::Body25Pose3D::FromAron(i.data());
                        }
                    }
                );
                if (not humanPose.has_value())
                {
                    ARMARX_DEBUG << "No human poses found.";
                    continue;
                }
            }

            ARMARX_TRACE;

            data.objects.forEachInstance(
                [&](const armem::wm::EntityInstance& i)
                {
                    armarx::armem::arondto::ObjectInstance object = armarx::armem::arondto::ObjectInstance::FromAron(i.data());

                    const std::string object_name = object.classID.entityName;

                    if (humanPose->keypoints.find("RWrist") == humanPose->keypoints.end())
                    {
                        return;
                    }

                    const auto [old_params, new_params] = [&]()
                    {
                        const Eigen::Vector3f pos = simox::math::mat4f_to_pos(object.pose.objectPoseRobot);
                        const Eigen::Vector3f ori = simox::math::mat4f_to_rpy(object.pose.objectPoseRobot);

                        const float height = pos.z();
                        const float width = pos.x();
                        const float pitch = ori.y();

                        const Eigen::Vector3f rhand = humanPose->keypoints["RWrist"].positionRobot.position;
                        const float distance = (pos - rhand).norm();

                        return add_and_filter_object_params(object_name, {.distance = distance, .height = height, .width = width, .pitch = pitch});
                    }();

                    if (i.metadata().timeCreated > last_processed_objects[object_name])
                    {
                        last_processed_objects[object_name] = i.metadata().timeCreated;

                        ARMARX_VERBOSE << "Object: " << object_name << ".";
                        ARMARX_VERBOSE << "Height: " << new_params.height << ".";
                        ARMARX_VERBOSE << "Width: " << new_params.width << ".";
                        ARMARX_VERBOSE << "Pitch: " << new_params.pitch << ".";
                        ARMARX_VERBOSE << "Distance: " << new_params.distance << ".";

                        if (new_params.distance < properties.contactBelow)
                        {
                            if (new_params.height - old_params.height > 10)
                            {
                                current_action = core::ManipulationAction{
                                    .action=core::Action{.name = "lift"},
                                    .manipulatedObject=armarx::ObjectID(object_name),
                                    .tool=std::nullopt
                                };
                            }
                            else if (new_params.width - old_params.width < -10)
                            {
                                current_action = core::ManipulationAction{
                                    .action=core::Action{.name = "place"},
                                    .manipulatedObject=armarx::ObjectID(object_name),
                                    .tool=std::nullopt
                                };
                            }
                        }
                    }
                    else if (new_params.distance > properties.releasedAbove)
                    {
                        if (current_action.has_value() and current_action->manipulatedObject == armarx::ObjectID(object_name))
                        {
                            current_action = core::ManipulationAction{
                                .action=core::Action{.name = "none"},
                                .manipulatedObject=armarx::ObjectID(object_name),
                                .tool=std::nullopt
                            };
                        }
                    }
                }
            );

            ARMARX_TRACE;

            const std::string action_name = action_to_string(current_action);
            if (currentSegment.label != action_name)
            {
                ARMARX_IMPORTANT << "Action now: " << action_name << ".";
                const long time = now.toMicroSeconds() - segmentation.referenceTime;
                currentSegment.endTime = time;
                segmentation.segments.push_back(currentSegment);
                currentSegment.beginTime = time;
                currentSegment.label = action_name;
                updateMemory(action_name);
            }
        }

        ARMARX_INFO << "Shutting down running task.";
    }


    armem::Time
    Component::getCurrentTime()
    {
        ARMARX_TRACE;

        if (properties.referenceTimeEnabled)
        {
            return armem::Time::now() - timeOffset;
        }

        return armem::Time::now();
    }


    Component::ReadMemoriesResult
    Component::readMemories(armem::Time time)
    {
        ARMARX_DEBUG << "Reading from memory for time " << time << ".";

        ARMARX_TRACE;

        const armem::client::QueryResult objectsResult = [&]
        {
            armem::client::query::Builder qb;

            qb
            .coreSegments().withName(properties.objectInstances.coreSegmentName)
            .providerSegments().all()
            .entities().all()
            //.snapshots().timeRange(now - armem::Time::seconds(1), now);
            .snapshots().beforeOrAtTime(time);

            return objectInstanceMemory.query(qb.buildQueryInput());
        }();

        ARMARX_TRACE;

        const armem::client::QueryResult humanPosesResult = [&]
        {
            armem::client::query::Builder qb;

            qb
            .coreSegments().withName(properties.humanPoses.coreSegmentName)
            .providerSegments().all()
            .entities().all()
            .snapshots().beforeOrAtTime(time);

            return humanPoseMemory.query(qb.buildQueryInput());
        }();

        ARMARX_TRACE;

        return {.objects = objectsResult.memory, .humanPoses = humanPosesResult.memory};
    }


    void
    Component::updateMemory(const std::string& action)
    {
        ARMARX_DEBUG << "Updating memory";

        ARMARX_TRACE;

        const armem::Time ts = armem::Time::now();

        ARMARX_TRACE;

        armem::EntityUpdate update;
        update.entityID = armem::MemoryID()
                              .withMemoryName(properties.recognizedActions.memoryName)
                              .withCoreSegmentName(properties.recognizedActions.coreSegmentName)
                              .withProviderSegmentName(getName())
                              .withEntityName("HumanIdentifier")
                              .withTimestamp(ts);
        update.timeCreated = ts;

        ARMARX_TRACE;

        // FIXME auto dto = core::toAron(event);

        ARMARX_TRACE;

        aron::data::DictPtr element(new aron::data::Dict);
        element->addElement("name", std::make_shared<aron::data::String>(action));
        // FIXME element->addElement("data", dto.toAron());

        update.instancesData = {element};

        ARMARX_TRACE;

        const armem::EntityUpdateResult updateResult = recognizedActionsMemory.commit(update);

        ARMARX_TRACE;

        if (not updateResult.success)
        {
            ARMARX_ERROR << updateResult.errorMessage;
        }

        ARMARX_DEBUG << "Done updating memory.";
    }


    /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
    void
    Component::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        // Setup the widgets.

        tab.boxLayerName.setValue(properties.boxLayerName);

        tab.numBoxes.setValue(properties.numBoxes);
        tab.numBoxes.setRange(0, 100);

        tab.drawBoxes.setLabel("Draw Boxes");

        // Setup the layout.

        GridLayout grid;
        int row = 0;
        {
            grid.add(Label("Box Layer"), {row, 0}).add(tab.boxLayerName, {row, 1});
            ++row;

            grid.add(Label("Num Boxes"), {row, 0}).add(tab.numBoxes, {row, 1});
            ++row;

            grid.add(tab.drawBoxes, {row, 0}, {2, 1});
            ++row;
        }

        VBoxLayout root = {grid, VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }


    void
    Component::RemoteGui_update()
    {
        if (tab.boxLayerName.hasValueChanged() || tab.numBoxes.hasValueChanged())
        {
            std::scoped_lock lock(propertiesMutex);
            properties.boxLayerName = tab.boxLayerName.getValue();
            properties.numBoxes = tab.numBoxes.getValue();

            {
                setDebugObserverDatafield("numBoxes", properties.numBoxes);
                setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
                sendDebugObserverBatch();
            }
        }
        if (tab.drawBoxes.wasClicked())
        {
            // Lock shared variables in methods running in seperate threads
            // and pass them to functions. This way, the called functions do
            // not need to think about locking.
            std::scoped_lock lock(propertiesMutex, arvizMutex);
            drawBoxes(properties, arviz);
        }
    }
    */


    /* (Requires the armarx::ArVizComponentPluginUser.)
    void
    Component::drawBoxes(const Component::Properties& p, viz::Client& arviz)
    {
        // Draw something in ArViz (requires the armarx::ArVizComponentPluginUser.
        // See the ArVizExample in RobotAPI for more examples.

        viz::Layer layer = arviz.layer(p.boxLayerName);
        for (int i = 0; i < p.numBoxes; ++i)
        {
            layer.add(viz::Box("box_" + std::to_string(i))
                      .position(Eigen::Vector3f(i * 100, 0, 0))
                      .size(20).color(simox::Color::blue()));
        }
        arviz.commit(layer);
    }
    */


    ARMARX_REGISTER_COMPONENT_EXECUTABLE(Component, Component::GetDefaultName());

}  // namespace armarx::programming_by_demonstration::components::bimanual_action_recognition
