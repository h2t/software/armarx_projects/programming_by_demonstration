/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    programming_by_demonstration::ArmarXObjects::pbd_cycle
 * @author     armar-user ( armar-user at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Component.h"

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

// Include headers you only need in function definitions in the .cpp.

// #include <Eigen/Core>

// #include <SimoxUtility/color/Color.h>
#include <SimoxUtility/algorithm/string.h>


namespace armarx::programming_by_demonstration::components::pbd_cycle
{

    const std::string
    Component::defaultName = "ttm_demo.pbd_cycle";


    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def = new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        // Publish to a topic (passing the TopicListenerPrx).
        // def->topic(myTopicListener);

        // Subscribe to a topic (passing the topic name).
        // def->topic<PlatformUnitListener>("MyTopic");

        // Use (and depend on) another component (passing the ComponentInterfacePrx).
        // def->component(myComponentProxy)

        def->component(ar, "ttm_demo.rule_based_ar", "cmp.action_recognition");
        def->component(imitation, "ttm_demo.imitation", "cmp.imitation");

        // Add a required property. (The component won't start without a value being set.)
        // def->required(properties.boxLayerName, "p.box.LayerName", "Name of the box layer in ArViz.");

        return def;
    }


    void
    Component::onInitComponent()
    {
        // Topics and properties defined above are automagically registered.

        // Keep debug observer data until calling `sendDebugObserverBatch()`.
        // (Requies the armarx::DebugObserverComponentPluginUser.)
        // setDebugObserverBatchModeEnabled(true);
    }


    void
    Component::onConnectComponent()
    {
        // Do things after connecting to topics and components.

        /* (Requies the armarx::DebugObserverComponentPluginUser.)
        // Use the debug observer to log data over time.
        // The data can be viewed in the ObserverView and the LivePlotter.
        // (Before starting any threads, we don't need to lock mutexes.)
        {
            setDebugObserverDatafield("numBoxes", properties.numBoxes);
            setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
            sendDebugObserverBatch();
        }
        */

        /* (Requires the armarx::ArVizComponentPluginUser.)
        // Draw boxes in ArViz.
        // (Before starting any threads, we don't need to lock mutexes.)
        drawBoxes(properties, arviz);
        */

        // Setup the remote GUI.
        {
            createRemoteGuiTab();
            RemoteGui_startRunningTask();
        }
    }


    void
    Component::onDisconnectComponent()
    {
    }


    void
    Component::onExitComponent()
    {
    }


    std::string
    Component::getDefaultName() const
    {
        return Component::defaultName;
    }


    std::string
    Component::GetDefaultName()
    {
        return Component::defaultName;
    }


    void
    Component::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        // Setup the widgets.

        tab.actions.setValue(properties.actions);
        tab.meets.setValue(properties.meetsConstraints);
        tab.before.setValue(properties.beforeConstraints);

        tab.startActionRecognition.setLabel("Start");
        tab.stopActionRecognition.setLabel("Stop");
        tab.startImitation.setLabel("Start");
        tab.stopImitation.setLabel("Stop");

        // Setup the layout.

        GridLayout grid;
        int row = 0;
        {
            grid.add(Label("Action Recognition"), {row, 0}).add(tab.startActionRecognition, {row, 1}).add(tab.stopActionRecognition, {row, 2});
            ++row;

            grid.add(Label("Actions"), {row, 0}).add(tab.actions, {row, 1}, {1, 2});
            ++row;

            grid.add(Label("Meets constraints"), {row, 0}).add(tab.meets, {row, 1}, {1, 2});
            ++row;

            grid.add(Label("Before constraints"), {row, 0}).add(tab.before, {row, 1}, {1, 2});
            ++row;

            grid.add(Label("Imitation"), {row, 0}).add(tab.startImitation, {row, 1}).add(tab.stopImitation, {row, 2});
            ++row;
        }

        VBoxLayout root = {grid, VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }


    void
    Component::RemoteGui_update()
    {
//        if (tab.boxLayerName.hasValueChanged() || tab.numBoxes.hasValueChanged())
//        {
//            std::scoped_lock lock(propertiesMutex);
//            properties.boxLayerName = tab.boxLayerName.getValue();
//            properties.numBoxes = tab.numBoxes.getValue();
//        }

//        if (tab.drawBoxes.wasClicked())
//        {
//            // Lock shared variables in methods running in seperate threads
//            // and pass them to functions. This way, the called functions do
//            // not need to think about locking.
//            std::scoped_lock lock(propertiesMutex, arvizMutex);
//            drawBoxes(properties, arviz);
//        }

        if (tab.startActionRecognition.wasClicked())
        {
            try
            {
                ar->start();
            }
            catch (...)
            {
                ARMARX_ERROR << "Failed starting action recognition.";
            }
        }

        if (tab.stopActionRecognition.wasClicked())
        {
            ttm_demo::rule_based_ar::Segmentation segmentation;
            try
            {
                segmentation = ar->stop();
            }
            catch (...)
            {
                ARMARX_ERROR << "Failed stopping action recognition.";
            }
        }

        if (tab.startImitation.wasClicked())
        {
            ttm_demo::imitation::ExecuteRequest request;

            std::vector<std::tuple<std::string, std::string>> action_object_list;

            auto getActionOnObject = [](const std::string object_action_str) -> ttm_demo::imitation::ActionOnObject
            {
                const std::vector<std::string> action_object = simox::alg::split(object_action_str, " ");
                ARMARX_CHECK_EQUAL(action_object.size(), 2) << "Expected pair of action and object ID (separated by a whitespace).";
                const std::string action = action_object[0];
                const std::string object = action_object[1];
                const std::vector<std::string> object_ds_class = simox::alg::split(object, "/");
                ARMARX_CHECK_EQUAL(action_object.size(), 2) << "Expected pair of dataset and object class name (separated by a slash).";
                const std::string dataset = object_ds_class[0];
                const std::string class_name = object_ds_class[1];
                return {.action=action, .objectID=data::ObjectID{.dataset=dataset, .className=class_name, .instanceName=""}};
            };

            const std::vector<std::string> action_object_list_str = simox::alg::split(properties.actions, ",");
            for (auto& object_action_str : action_object_list_str)
            {
                request.actions.push_back(getActionOnObject(object_action_str));
            }

            const std::vector<std::string> meets_constraints_str = simox::alg::split(properties.meetsConstraints, ",");
            for (const std::string& meets_constraint_str : meets_constraints_str)
            {
                const std::vector<std::string> object_pair_str = simox::alg::split(meets_constraint_str, ">");
                ARMARX_CHECK_EQUAL(object_pair_str.size(), 2) << "Expected pair of actions (separated by a '>').";
                const ttm_demo::imitation::ActionOnObject action1 = getActionOnObject(object_pair_str[0]);
                const ttm_demo::imitation::ActionOnObject action2 = getActionOnObject(object_pair_str[1]);
                const ttm_demo::imitation::Constraint c{.action1=action1, .action2=action2};
                request.meets.push_back(c);
            }

            const std::vector<std::string> before_constraints_str = simox::alg::split(properties.beforeConstraints, ",");
            for (const std::string& before_constraint_str : before_constraints_str)
            {
                const std::vector<std::string> object_pair_str = simox::alg::split(before_constraint_str, ">");
                ARMARX_CHECK_EQUAL(object_pair_str.size(), 2) << "Expected pair of actions (separated by a '>').";
                const ttm_demo::imitation::ActionOnObject action1 = getActionOnObject(object_pair_str[0]);
                const ttm_demo::imitation::ActionOnObject action2 = getActionOnObject(object_pair_str[1]);
                const ttm_demo::imitation::Constraint c{.action1=action1, .action2=action2};
                request.before.push_back(c);
            }

            try
            {
                imitation->start(request);
            }
            catch (...)
            {
                ARMARX_ERROR << "Failed starting imitation execution.";
            }
        }

        if (tab.stopImitation.wasClicked())
        {
            try
            {
                imitation->stop();
            }
            catch (...)
            {
                ARMARX_ERROR << "Failed stopping imitation execution.";
            }
        }
    }

    /* (Requires the armarx::ArVizComponentPluginUser.)
    void
    Component::drawBoxes(const Component::Properties& p, viz::Client& arviz)
    {
        // Draw something in ArViz (requires the armarx::ArVizComponentPluginUser.
        // See the ArVizExample in RobotAPI for more examples.

        viz::Layer layer = arviz.layer(p.boxLayerName);
        for (int i = 0; i < p.numBoxes; ++i)
        {
            layer.add(viz::Box("box_" + std::to_string(i))
                      .position(Eigen::Vector3f(i * 100, 0, 0))
                      .size(20).color(simox::Color::blue()));
        }
        arviz.commit(layer);
    }
    */


    ARMARX_REGISTER_COMPONENT_EXECUTABLE(Component, Component::GetDefaultName());

}  // namespace armarx::programming_by_demonstration::components::pbd_cycle
