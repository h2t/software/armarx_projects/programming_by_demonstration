/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * package    programming_by_demonstration::ttm_demo_imitation
 * author     armar-user ( armar-user at kit dot edu )
 * date       2022
 * copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *            GNU General Public License
 */


#pragma once


#include <RobotAPI/interface/ArmarXObjects/ArmarXObjectsTypes.ice>
#include <RobotAPI/interface/armem/client/MemoryListenerInterface.ice>


module armarx {  module programming_by_demonstration {  module components {  module ttm_demo { module imitation
{

    struct ActionOnObject
    {
        string action;
        data::ObjectID objectID;
    };

    sequence<ActionOnObject> ActionsOnObjects;

    struct Constraint
    {
        ActionOnObject action1;
        ActionOnObject action2;
    };

    sequence<Constraint> Constraints;

    struct ExecuteRequest
    {
        ActionsOnObjects actions;
        Constraints meets;
        Constraints before;
    };

    interface ComponentInterface extends
            armarx::armem::client::MemoryListenerInterface
    {
        void start(ExecuteRequest request);
        void executeGrasp();
        void stop();
    };

};};};};};
