/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    programming_by_demonstration::ArmarXObjects::ttm_demo_imitation
 * @author     armar-user ( armar-user at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Component.h"

#include <thread>

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>
#include <ArmarXCore/util/time.h>
#include <ArmarXCore/core/application/properties/PluginEigen.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectPose.h>
#include <RobotAPI/libraries/ArmarXObjects/ice_conversions.h>
#include <RobotAPI/libraries/core/Pose.h>

// Include headers you only need in function definitions in the .cpp.

// #include <Eigen/Core>

// #include <SimoxUtility/color/Color.h>
#include <SimoxUtility/algorithm/string.h>
#include <SimoxUtility/math.h>
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/math/Helpers.h>

#include <mobile_manipulation/control/NJointTaskSpaceImpedanceControllerVelocityLimited.h>


namespace armarx::programming_by_demonstration::components::ttm_demo::imitation
{

    const std::string
    Component::defaultName = "ttm_demo.imitation";


    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def = new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        // Publish to a topic (passing the TopicListenerPrx).
        // def->topic(myTopicListener);

        // Subscribe to a topic (passing the topic name).
        // def->topic<PlatformUnitListener>("MyTopic");

        // Use (and depend on) another component (passing the ComponentInterfacePrx).
        // def->component(myComponentProxy)

        // Add a required property. (The component won't start without a value being set.)
        // def->required(properties.boxLayerName, "p.box.LayerName", "Name of the box layer in ArViz.");

        // Add an optionalproperty.

        def->component(rpc.objectPoseProvider);
        def->component(rpc.pickAndPlace);
        def->component(rpc.rightHandUnit, "RightHandUnit");

        def->optional(properties.impedanceKpos, "impedance.Kpos");
        def->optional(properties.impedanceDpos, "impedance.Dpos");
        def->optional(properties.impedanceApproachLinearVel, "impedance.approach.linearVel");
        def->optional(properties.impedanceRetreatLinearVel, "impedance.retreat.linearVel");
        def->optional(properties.dryRun, "dryRun", "Do not actually execute anything.");
        def->optional(properties.startupTimeout, "startupTimeout", "Time to wait before doing anything on imitate call.");
        def->optional(properties.objectRequestPeriod, "objectRequestPeriod", "Time to wait for object pose provider to deliver poses.");
        def->optional(properties.objectSearchTimeout, "objectSearchTimeout", "Time to actively query for object poses before giving up.");
        def->optional(properties.retreatTimeout, "retreatTimeout", "Time to wait after opening hand for placing before retreating.");
        def->optional(properties.placingPosReachedThreshold, "placingPosReachedThreshold");
        def->optional(properties.approachHaldClosure, "approachHandClosure");

        robotReader.registerPropertyDefinitions(def);

        return def;
    }


    Component::Component() :
        robotReader(memoryNameSystem())
    {
        ;
    }


    void
    Component::onInitComponent()
    {
        // Topics and properties defined above are automagically registered.

        // Keep debug observer data until calling `sendDebugObserverBatch()`.
        // (Requies the armarx::DebugObserverComponentPluginUser.)
        // setDebugObserverBatchModeEnabled(true);
    }


    void
    Component::onConnectComponent()
    {
        robotReader.connect();
        robotReader.setSyncTimeout(armarx::Duration::MilliSeconds(100));
        robotReader.setSleepAfterSyncFailure(armarx::Duration::MilliSeconds(10));

        // initialize robot
        robot = robotReader.getRobot("Armar6", armarx::armem::Time::Now());

        rightHandHelper =
            std::make_unique<armarx::simple::Armar6HandV2Helper>(rpc.rightHandUnit, "Right");

        /* (Requies the armarx::DebugObserverComponentPluginUser.)
        // Use the debug observer to log data over time.
        // The data can be viewed in the ObserverView and the LivePlotter.
        // (Before starting any threads, we don't need to lock mutexes.)
        {
            setDebugObserverDatafield("numBoxes", properties.numBoxes);
            setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
            sendDebugObserverBatch();
        }
        */

        /* (Requires the armarx::ArVizComponentPluginUser.)
        // Draw boxes in ArViz.
        // (Before starting any threads, we don't need to lock mutexes.)
        drawBoxes(properties, arviz);
        */

        // Setup the remote GUI.
        {
            createRemoteGuiTab();
            RemoteGui_startRunningTask();
        }
    }


    inline armarx::NJointTaskSpaceImpedanceControlRuntimeConfig
    toRuntimeConfig(const armarx::NJointTaskSpaceImpedanceControlConfig& cfg)
    {
        return armarx::NJointTaskSpaceImpedanceControlRuntimeConfig{.Kpos = cfg.Kpos,
                                                                    .Kori = cfg.Kori,
                                                                    .Dpos = cfg.Dpos,
                                                                    .Dori = cfg.Dori,
                                                                    .desiredJointPositions =
                                                                        cfg.desiredJointPositions,
                                                                    .Knull = cfg.Knull,
                                                                    .Dnull = cfg.Dnull,
                                                                    .torqueLimit = cfg.torqueLimit};
    }


    void
    Component::activateImpedanceController(const std::string& rnsName)
    {
        if (properties.dryRun)
        {
            ARMARX_INFO << "Won't activate impendance controller in simulation";
        }

        const std::string impedanceControllerName = getName() + "_GraspingImpedanceController_" + rnsName;

        auto rns = robot->getRobotNodeSet(rnsName);
        std::vector<float> initialJointPositions = rns->getJointValues();

        auto tcp = rns->getTCP();

        const auto initialTcpPose = tcp->getPoseInRootFrame();
        //ARMARX_INFO << "Impedance control TCP position "
        //            << Eigen::Isometry3f(initialTcpPose).translation();

        // Impedance Control
        //const std::vector<float> Kpos = {500, 1000, 1000};
        const std::vector<float> Kpos = {properties.impedanceKpos.x(), properties.impedanceKpos.y(), properties.impedanceKpos.z()};

        const std::vector<float> Kori = {10, 10, 10};

        //const std::vector<float> Dpos = {800, 800, 800};
        const std::vector<float> Dpos = {properties.impedanceDpos.x(), properties.impedanceDpos.y(), properties.impedanceDpos.z()};

        const std::vector<float> Dori = {50, 50, 50};
        float knullval = 3;
        float dnullval = 1;

        std::vector<float> knull;
        std::vector<float> dnull;

        for (size_t i = 0; i < 8; ++i)
        {
            knull.push_back(knullval);
            dnull.push_back(dnullval);
        }

          // the shoulder should be allowed to move
        knull.at(0) = 0.1;
        dnull.at(0) = 0.03;

         // but the shoulder 2 not that much
        knull.at(1) *= 5;
        dnull.at(1) *= 5;

          // the wrist violates the joint limits so increase the gain to "avoid" this
        knull.at(6) *= 10;
        dnull.at(6) *= 10;

        // std::vector<float> desiredJointPositionRight = robot->getRobotNodeSet("RightArm")->getJointValues();

        armarx::NJointTaskSpaceImpedanceControlConfigPtr controllerConfig =
            new armarx::NJointTaskSpaceImpedanceControlConfig;
        controllerConfig->nodeSetName = rnsName;
        controllerConfig->desiredPosition = math::Helpers::Position(initialTcpPose);
        controllerConfig->desiredOrientation = math::Helpers::Orientation(initialTcpPose);
        controllerConfig->Kpos = Eigen::Map<const Eigen::Vector3f>(Kpos.data(), Kpos.size());
        controllerConfig->Kori = Eigen::Map<const Eigen::Vector3f>(Kori.data(), Kori.size());
        controllerConfig->Dpos = Eigen::Map<const Eigen::Vector3f>(Dpos.data(), Dpos.size());
        controllerConfig->Dori = Eigen::Map<const Eigen::Vector3f>(Dori.data(), Dori.size());
        controllerConfig->desiredJointPositions = Eigen::Map<const Eigen::VectorXf>(
            initialJointPositions.data(), initialJointPositions.size());
        controllerConfig->Knull = Eigen::Map<const Eigen::VectorXf>(knull.data(), knull.size());
        controllerConfig->Dnull = Eigen::Map<const Eigen::VectorXf>(dnull.data(), dnull.size());
        controllerConfig->torqueLimit = 15;

        armarx::NJointControllerInterfacePrx controller =
            getRobotUnit()->getNJointController(impedanceControllerName);
        if (controller)
        {
            ARMARX_INFO << "Reusing controller";
            taskSpaceImpedanceController =
                armarx::NJointTaskSpaceImpedanceControlInterfacePrx::checkedCast(controller);

            ARMARX_CHECK_NOT_NULL(taskSpaceImpedanceController);

            const auto runtimeCfg = toRuntimeConfig(*controllerConfig);
            taskSpaceImpedanceController->setConfig(runtimeCfg);
        }
        else
        {
            controller = getRobotUnit()->createNJointController(
                "NJointTaskSpaceImpedanceController", impedanceControllerName, controllerConfig);

            ARMARX_INFO << "Created controller";
        }

        taskSpaceImpedanceController =
            armarx::NJointTaskSpaceImpedanceControlInterfacePrx::checkedCast(controller);
        ARMARX_CHECK_NOT_NULL(taskSpaceImpedanceController);

        // FIXME ARMARX_CHECK(not params.isSimulation) << "Cannot use impedance control in simulation!";
        // in simulation, this is not possible ...

        taskSpaceImpedanceController->activateController();
    }


    void
    Component::onDisconnectComponent()
    {
        ;
    }


    void
    Component::onExitComponent()
    {
        ;
    }


    std::string
    Component::getDefaultName() const
    {
        return Component::defaultName;
    }


    std::string
    Component::GetDefaultName()
    {
        return Component::defaultName;
    }


    void
    Component::run()
    {
        ARMARX_INFO << "Received execution request.";

        struct Grasp
        {
            std::string id;
            grasping::GraspCandidatePtr grasp;
            std::string side;
        };

        Properties properties;
        {
            std::lock_guard l{propertiesMutex};
            properties = this->properties;
        }

        ARMARX_CHECK_GREATER(request.actions.size(), 0) << "No actions in imitation request.";

        auto planGrasp = [this](const armarx::objpose::ObjectPose& objectPose) -> std::optional<Grasp>
        {
            std::string side = "Right";

            grasping::CalculateGraspCandidatesInput input;
            toIce(input.objectPose, objectPose);
            input.side = side;
            grasping::CalculateGraspCandidatesOutput output = rpc.pickAndPlace->calculateGraspCandidates(input);
            std::string bestId;
            grasping::GraspCandidatePtr bestCandidate;
            for (const auto& [id, candidate] : output.candidates)
            {
                ARMARX_CHECK_NOT_NULL(candidate) << VAROUT(id);
                ARMARX_CHECK_NOT_NULL(candidate->reachabilityInfo) << VAROUT(id);
                if (candidate->reachabilityInfo->reachable)
                {
                    ARMARX_DEBUG << candidate->approachVector->x << "," << candidate->approachVector->y << "," << candidate->approachVector->z;
                    if (fromIce(candidate->approachVector).z() < 0.5)
                    {
                        continue;
                    }
                    if (not bestCandidate)
                    {
                        bestId = id;
                        bestCandidate = candidate;
                    }
                    else if (candidate->graspPose->position->z > bestCandidate->graspPose->position->z)
                    {
                        bestId = id;
                        bestCandidate = candidate;
                    }
                }
            }

            if (bestCandidate)
            {
                // Take it.
                ARMARX_INFO << "Found reachable grasp '" << bestId << "' "
                            << "(side '" << side << "').";
                return Grasp{bestId, bestCandidate, side};
            }

            return std::nullopt;
        };
        auto aoo_to_string = [](const ActionOnObject& aoo) {
            return aoo.action + " " + aoo.objectID.dataset + "/" + aoo.objectID.className;
        };
        auto object_id_to_string = [](const data::ObjectID& object_id) {
            return object_id.dataset + "/" + object_id.className;
        };

        // Debug.
        {
            for (const ActionOnObject& action : request.actions)
            {
                ARMARX_DEBUG << "Got action: '" << aoo_to_string(action) << "'.";
            }
            for (const Constraint& constraint : request.meets)
            {
                ARMARX_DEBUG << "Got meets constraint between '" << aoo_to_string(constraint.action1) << "' and '" << aoo_to_string(constraint.action2) << "'.";
            }
            for (const Constraint& constraint : request.before)
            {
                ARMARX_DEBUG << "Got before constraint between '" << aoo_to_string(constraint.action1) << "' and '" << aoo_to_string(constraint.action2) << "'.";
            }
        }

        std::optional<Grasp> grasp;

        std::map<std::string, bool> actionCompletions;
        std::map<std::string, bool> actionFailures;
        for (const ActionOnObject& actionOnObject : request.actions)
        {
            actionCompletions[aoo_to_string(actionOnObject)] = false;
            actionFailures[aoo_to_string(actionOnObject)] = false;
        }
        auto taskCompleted = [&actionCompletions] {
            for (auto& [k, v] : actionCompletions)
            {
                if (not v)
                {
                    return false;
                }
            }

            return true;
        };
        auto anyActionFailures = [&actionFailures]()
        {
            bool any = false;
            for (auto& [k, v] : actionFailures)
            {
                if (v)
                {
                    any = true;
                }
            }
            return any;
        };
        auto clearActionFailures = [&actionFailures]()
        {
            for (auto& [k, v] : actionFailures)
            {
                actionFailures[k] = false;
            }
        };

        auto findCandidate = [&](const std::optional<ActionOnObject>& previous) -> std::optional<ActionOnObject>
        {
            // If previous was completed successfully, check if a meets-constraint simplifies the search for a candidate.
            if (previous.has_value() and actionCompletions[aoo_to_string(previous.value())])
            {
                ARMARX_VERBOSE << "Previous action was completed.";
                for (const Constraint& constraint : request.meets)
                {
                    if (constraint.action1 == previous.value())
                    {
                        return constraint.action2;
                    }
                }
            }

            // Check all actions if their prerequisites are met. If so, use first match as candidate.
            for (const ActionOnObject& candidate : request.actions)
            {
                // If candiate was already completed => continue.
                if (actionCompletions[aoo_to_string(candidate)])
                {
                    continue;
                }

                bool constrained = false;

                for (const Constraint& constraint : request.before)
                {
                    // If action1 of constraint must happen before candidate and action1 is not completed => constrained.
                    if (constraint.action2 == candidate and not actionCompletions[aoo_to_string(constraint.action1)])
                    {
                        constrained = true;
                        break;
                    }
                }
                for (const Constraint& constraint : request.meets)
                {
                    // If action1 of constraint must meet candidate and action1 is not completed => constrained.
                    if (constraint.action2 == candidate and not actionCompletions[aoo_to_string(constraint.action1)])
                    {
                        constrained = true;
                        break;
                    }
                }

                ARMARX_VERBOSE << "Checking if '" << aoo_to_string(candidate) << "' is constrained: " << constrained << ".";

                if (not constrained and not actionFailures[aoo_to_string(candidate)])
                {
                    return candidate;
                }
            }

            ARMARX_VERBOSE << "Could not find any candidate, constraints too restrictive.";
            return std::nullopt;
        };

        if (not properties.dryRun)
        {
            // TODO: lol.
            std::map<std::string, float> jointValues;
            jointValues["Neck_1_Yaw"] = VirtualRobot::MathTools::deg2rad(-20);
            jointValues["Neck_2_Pitch"] = VirtualRobot::MathTools::deg2rad(45);
            jointValues["ArmR1_Cla1"] = -0.70137244462966919;
            jointValues["ArmR2_Sho1"] = 0.26838809251785278;
            jointValues["ArmR3_Sho2"] = 0.45938742160797119;
            jointValues["ArmR4_Sho3"] = -0.040434014052152634;
            jointValues["ArmR5_Elb1"] = 1.911098837852478;
            jointValues["ArmR6_Elb2"] = -1.5841352939605713;
            jointValues["ArmR7_Wri1"] = -0.27602747082710266;
            jointValues["ArmR8_Wri2"] = 0.5625426173210144;
            std::map<std::string, armarx::ControlMode> ctrlModes;
            for (auto& [k, v] : jointValues)
            {
                ctrlModes[k] = ePositionControl;
            }

            rightHandHelper->openHand();
            rightHandHelper->closeHand();
            rightHandHelper->openHand();

            ARMARX_INFO << "Looking straight";
            getRobotUnit()->getKinematicUnit()->switchControlMode(ctrlModes);
            getRobotUnit()->getKinematicUnit()->setJointAngles(jointValues);
        }

        // SimTrack.
        {
            std::vector<data::ObjectID> objectIDs;
            for (const auto& action : request.actions)
            {
                objectIDs.push_back(action.objectID);
            }
            objpose::provider::RequestObjectsInput req;
            req.objectIDs = objectIDs;
            req.relativeTimeoutMS = -1;
            rpc.objectPoseProvider->requestObjects(req);
        }

        if (properties.startupTimeout.toMilliSeconds() > 0)
        {
            ARMARX_INFO << "Waiting " << properties.startupTimeout << " before continuing.";
            std::this_thread::sleep_for(std::chrono::milliseconds(properties.startupTimeout.toMilliSeconds()));
            ARMARX_INFO << "Starting now.";
        }

        std::optional<ActionOnObject> currentAction;

        do
        {
            currentAction = findCandidate(currentAction);

            if (not currentAction.has_value())
            {
                if (anyActionFailures())
                {
                    ARMARX_VERBOSE << "Exhausted possibilites, clearing localization failure lists and re-trying.";
                    clearActionFailures();
                    currentAction = findCandidate(currentAction);
                }

                if (not currentAction.has_value())
                {
                    ARMARX_VERBOSE << "Could not find a viable next candidate.";
                    break;
                }
            }
            else
            {
                // Not necessary, but will result in corny->milk->soy-drink.
                clearActionFailures();
            }

            const std::string full_name = aoo_to_string(currentAction.value());
            const std::string name = object_id_to_string(currentAction->objectID);

            ARMARX_INFO << "Try executing '" << full_name << "'.";

            if (currentAction->action == "lift")
            {
                ARMARX_INFO << "Querying memory for current pose reportings of object with ID '" << name << "'.";
                std::optional<armarx::objpose::ObjectPose> targetObjectPose;
                {
                    armarx::CycleUtil cu{IceUtil::Time::milliSeconds(100)};
                    const IceUtil::Time deadline = IceUtil::Time::now() + properties.objectSearchTimeout;
                    bool notFound = true;
                    do
                    {
                        cu.waitForCycleDuration();

                        for (const objpose::ObjectPose& pose : getObjectPoses())
                        {
                            if (pose.objectID.dataset() == currentAction->objectID.dataset
                                    and pose.objectID.className() == currentAction->objectID.className)
                            {
                                if (pose.timestamp + armarx::Duration::Seconds(1) > armarx::Clock::Now())
                                {
                                    ARMARX_VERBOSE << "Found object.";
                                    targetObjectPose = pose;
                                    notFound = false;
                                    break;
                                }
                                else
                                {
                                    ARMARX_DEBUG << "Found object, but it is " << armarx::Clock::Now() - pose.timestamp << " old.";
                                }
                            }
                        }
                    }
                    while (IceUtil::Time::now() <= deadline and notFound);
                }

                if (not targetObjectPose.has_value())
                {
                    ARMARX_WARNING << "Reached timeout while searching for object '" << name << "'.";
                    actionFailures[full_name] = true;
                    continue;
                }

                grasp = planGrasp(targetObjectPose.value());

                if (not grasp.has_value())
                {
                    ARMARX_WARNING << "Grasp planning failed. Cannot grasp the target object.";
                    actionFailures[full_name] = true;
                    continue;
                }

                graspAndLift(grasp.value().grasp);
            }
            else
            {
                if (not grasp.has_value())
                {
                    ARMARX_ERROR << "No object was grasped, cannot place one.";
                    break;
                }

                // Place.
                if (not properties.dryRun)
                {
                    const Eigen::Vector3f pos{-129, 744, 1098};
                    const Eigen::Quaternionf ori{0.7056807279586792, -0.17401625216007233, -0.66286152601242065, -0.17985528707504272};
                    Eigen::Isometry3f pose = Eigen::Isometry3f::Identity();
                    pose.translation() = pos;
                    pose.linear() = Eigen::AngleAxisf(ori).toRotationMatrix();
                    ARMARX_INFO << "Execute place action.";
                    const std::string rnsName = "RightArm";
                    activateImpedanceController(rnsName);
                    taskSpaceImpedanceController->setPose(pose.matrix());
                    armarx::CycleUtil cu{IceUtil::Time::milliSeconds(100)};
                    bool target_reached = false;
                    do
                    {
                        ARMARX_CHECK(robotReader.synchronizeRobot(*robot, armarx::armem::Time::Now()));
                        const Eigen::Isometry3f robot_T_tcp_current(robot->getRobotNodeSet(rnsName)->getTCP()->getPoseInRootFrame());
                        const Eigen::Vector3f posDiff = pos - robot_T_tcp_current.translation();
                        target_reached = posDiff.norm() < properties.placingPosReachedThreshold;
                        ARMARX_DEBUG << "Placing, difference is: " << posDiff.norm() << ".";
                        cu.waitForCycleDuration();
                    }
                    while (not target_reached);
                    rightHandHelper->openHand();
                    std::this_thread::sleep_for(std::chrono::milliseconds(properties.retreatTimeout.toMilliSeconds()));
                }
                else
                {
                    ARMARX_IMPORTANT << "Dry run, not executing place.";
                }

                // Retreat.
                if (not properties.dryRun)
                {
                    const Eigen::Vector3f pos{567, 475, 1028};
                    const Eigen::Quaternionf ori{0.54344063997268677, -0.45742309093475342, -0.4817969799041748, -0.51313591003417969};
                    Eigen::Isometry3f pose = Eigen::Isometry3f::Identity();
                    pose.translation() = pos;
                    pose.linear() = Eigen::AngleAxisf(ori).toRotationMatrix();
                    ARMARX_INFO << "Execute place action.";
                    const std::string rnsName = "RightArm";
                    activateImpedanceController(rnsName);
                    taskSpaceImpedanceController->setPose(pose.matrix());
                    armarx::CycleUtil cu{IceUtil::Time::milliSeconds(100)};
                    bool target_reached = false;
                    do
                    {
                        ARMARX_CHECK(robotReader.synchronizeRobot(*robot, armarx::Clock::Now()));
                        const Eigen::Isometry3f robot_T_tcp_current(robot->getRobotNodeSet(rnsName)->getTCP()->getPoseInRootFrame());
                        const Eigen::Vector3f posDiff = pos - robot_T_tcp_current.translation();
                        target_reached = posDiff.norm() < properties.placingPosReachedThreshold;
                        ARMARX_DEBUG << "Retreating, difference is: " << posDiff.norm() << ".";
                        cu.waitForCycleDuration();
                    }
                    while (not target_reached);
                }
                else
                {
                    ARMARX_IMPORTANT << "Dry run, not executing retreat.";
                }
            }

            ARMARX_INFO << "Executed '" << full_name << "'.";
            actionCompletions[full_name] = true;
        }
        while (not taskCompleted());

        // SimTrack (unrequest).
        {
            std::vector<data::ObjectID> objectIDs;
            for (const auto& action : request.actions)
            {
                objectIDs.push_back(action.objectID);
            }
            objpose::provider::RequestObjectsInput req;
            req.objectIDs = objectIDs;
            req.relativeTimeoutMS = 1;
            rpc.objectPoseProvider->requestObjects(req);
        }
    }


    void
    Component::graspAndLift(grasping::GraspCandidatePtr candidate)
    {
        using namespace mobile_manipulation;

        if (properties.dryRun)
        {
            ARMARX_IMPORTANT << "Dry run, not executing grasp and lift.";
            return;
        }

        const Eigen::Vector3f robot_V_tcp_approach =
            armarx::fromIce(candidate->approachVector);
        Eigen::Isometry3f robot_T_graspTcp{armarx::fromIce(candidate->graspPose)};
        robot_T_graspTcp.translation().z() = std::max(800.f, robot_T_graspTcp.translation().z());
        Eigen::Isometry3f robot_T_approach = robot_T_graspTcp;
        //robot_T_approach.translation().z() += 300;
        robot_T_approach.translation() += (robot_V_tcp_approach * 300);
        const Eigen::Isometry3f robot_T_lift = robot_T_approach;

        const std::string rnsName = "RightArm";

        ARMARX_CHECK(robotReader.synchronizeRobot(*robot, armarx::Clock::Now()));

        activateImpedanceController(rnsName);

        syncRobotTask = new armarx::SimplePeriodicTask(
            [this]() {
                ARMARX_CHECK(robotReader.synchronizeRobot(*robot, armarx::Clock::Now()));
            },
            20);
        syncRobotTask->start();

        {
            {
                control::NJointTaskSpaceImpedanceControllerVelocityLimited::Params params_apprach{
                    .posDiffEps = static_cast<float>(properties.placingPosReachedThreshold),
                    .linearVelocity = properties.impedanceApproachLinearVel,
                    .updateRateMS = 10};

                control::NJointTaskSpaceImpedanceControllerVelocityLimited
                    impedanceControllerVelLimited(taskSpaceImpedanceController,
                                                  robot->getRobotNodeSet(rnsName)->getTCP(),
                                                  params_apprach);

                taskSpaceImpedanceController->setPose(robot_T_approach.matrix());

                rightHandHelper->shapeHand(properties.approachHaldClosure, 0);

                std::this_thread::sleep_for(std::chrono::seconds(5));

                for (const auto& robot_T_currentTcpTarget : {robot_T_approach, robot_T_graspTcp})
                {
                    const Eigen::Vector3f robot_P_tcp_target = robot_T_currentTcpTarget.translation();

                    impedanceControllerVelLimited.setTarget(robot_P_tcp_target);
                    impedanceControllerVelLimited.run();
                }
            }

            rightHandHelper->closeHand();

            {
                control::NJointTaskSpaceImpedanceControllerVelocityLimited::Params params_retreat{
                    .posDiffEps = static_cast<float>(properties.placingPosReachedThreshold),
                    .linearVelocity = properties.impedanceRetreatLinearVel,
                    .updateRateMS = 10};

                control::NJointTaskSpaceImpedanceControllerVelocityLimited
                    impedanceControllerVelLimited(taskSpaceImpedanceController,
                                                  robot->getRobotNodeSet(rnsName)->getTCP(),
                                                  params_retreat);

                const Eigen::Vector3f robot_P_tcp_target = robot_T_lift.translation();
                    impedanceControllerVelLimited.setTarget(robot_P_tcp_target);
                    impedanceControllerVelLimited.run();
            }

            std::this_thread::sleep_for(std::chrono::milliseconds(1000));


        }

        taskSpaceImpedanceController->deactivateController();

        syncRobotTask->stop();
        syncRobotTask = nullptr;
    }


    void
    Component::start(const ttm_demo::imitation::ExecuteRequest& request, const Ice::Current&)
    {
        if (not task or not task->isRunning())
        {
            this->request = request;
            task = new RunningTask<Component>(this, &Component::run);
            ARMARX_INFO << "Starting imitation task.";
            task->start();
            ARMARX_INFO << "Started imitation task.";
        }
    }


    void
    Component::executeGrasp(const Ice::Current&)
    {

    }


    void
    Component::stop(const Ice::Current&)
    {
        if (task and task->isRunning())
        {
            const bool join = true;
            ARMARX_INFO << "Stopping action recognition task.";
            task->stop(join);
            ARMARX_INFO << "Stopped imitation task.imitation.";
        }
    }


    void
    Component::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        // Setup the widgets.

        tab.startupTimeout.setRange(0, 10000);
        tab.startupTimeout.setValue(properties.startupTimeout.toMilliSeconds());
        tab.objectRequestPeriod.setRange(0, 10000);
        tab.objectRequestPeriod.setValue(properties.objectRequestPeriod.toMilliSeconds());
        tab.objectSearchTimeout.setRange(0, 10000);
        tab.objectSearchTimeout.setValue(properties.objectSearchTimeout.toMilliSeconds());
        tab.retreatTimeout.setRange(0, 10000);
        tab.retreatTimeout.setValue(properties.retreatTimeout.toMilliSeconds());

        // Setup the layout.

        GridLayout grid;
        int row = 0;
        {
            grid.add(Label("Startup timeout [ms]."), {row, 0}).add(tab.startupTimeout, {row, 1});
            ++row;
            grid.add(Label("Object request period [ms]."), {row, 0}).add(tab.objectRequestPeriod, {row, 1});
            ++row;
            grid.add(Label("Object search timeout [ms]."), {row, 0}).add(tab.objectSearchTimeout, {row, 1});
            ++row;
            grid.add(Label("Retreat timeout [ms]."), {row, 0}).add(tab.retreatTimeout, {row, 1});
            ++row;
        }

        VBoxLayout root = {grid, VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }


    void
    Component::RemoteGui_update()
    {
        if (tab.startupTimeout.hasValueChanged())
        {
            std::scoped_lock lock(propertiesMutex);
            properties.startupTimeout = IceUtil::Time::milliSeconds(tab.startupTimeout.getValue());
        }

        if (tab.objectRequestPeriod.hasValueChanged())
        {
            std::scoped_lock lock(propertiesMutex);
            properties.objectRequestPeriod = IceUtil::Time::milliSeconds(tab.objectRequestPeriod.getValue());
        }

        if (tab.objectSearchTimeout.hasValueChanged())
        {
            std::scoped_lock lock(propertiesMutex);
            properties.objectSearchTimeout = IceUtil::Time::milliSeconds(tab.objectSearchTimeout.getValue());
        }

        if (tab.retreatTimeout.hasValueChanged())
        {
            std::scoped_lock lock(propertiesMutex);
            properties.retreatTimeout = IceUtil::Time::milliSeconds(tab.retreatTimeout.getValue());
        }
    }


    /* (Requires the armarx::ArVizComponentPluginUser.)
    void
    Component::drawBoxes(const Component::Properties& p, viz::Client& arviz)
    {
        // Draw something in ArViz (requires the armarx::ArVizComponentPluginUser.
        // See the ArVizExample in RobotAPI for more examples.

        viz::Layer layer = arviz.layer(p.boxLayerName);
        for (int i = 0; i < p.numBoxes; ++i)
        {
            layer.add(viz::Box("box_" + std::to_string(i))
                      .position(Eigen::Vector3f(i * 100, 0, 0))
                      .size(20).color(simox::Color::blue()));
        }
        arviz.commit(layer);
    }
    */


    ARMARX_REGISTER_COMPONENT_EXECUTABLE(Component, Component::GetDefaultName());

}  // namespace armarx::programming_by_demonstration::components::ttm_demo_imitation
