cmake_minimum_required(VERSION 3.18)

find_package(ArmarXCore REQUIRED)
include(${ArmarXCore_USE_FILE})

set(ARMARX_ENABLE_DEPENDENCY_VERSION_CHECK_DEFAULT FALSE)

# Project definition.
armarx_enable_modern_cmake_project()  # Temporary until migration of ArmarXCore.
armarx_project(programming_by_demonstration)

add_subdirectory(etc)

# Required ArmarX dependencies.
armarx_find_package(PUBLIC MPLIB REQUIRED)
armarx_find_package(PUBLIC RobotAPI REQUIRED)
armarx_find_package(PUBLIC MemoryX REQUIRED)
armarx_find_package(PUBLIC VisionX REQUIRED)
armarx_find_package(PUBLIC armar6::skills REQUIRED)
armarx_find_package(PUBLIC mobile_manipulation REQUIRED)

# Optional ArmarX dependencies.
armarx_find_package(PUBLIC ArmarXGui QUIET)

# Required system dependencies.
# armarx_find_package(PUBLIC Boost REQUIRED)

# Optional system dependencies.
# armarx_find_package(PUBLIC PCL QUIET)

add_subdirectory(source/programming_by_demonstration)

armarx_install_project()
